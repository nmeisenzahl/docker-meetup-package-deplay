#!/bin/bash
# Example - Docker build with Kaniko on Kubernetes (no push)

kubectl apply -f - << EOF
apiVersion: v1
kind: Pod
metadata:
  name: kaniko
spec:
  containers:
  - name: kaniko
    image: gcr.io/kaniko-project/executor:latest
    args: ["--dockerfile=./Dockerfile",
            "--context=/workspace",
            "--no-push"]
    volumeMounts:
      - name: workspace
        mountPath: /workspace
  restartPolicy: Never
  volumes:
    - name: workspace
      hostPath:
        path: $(pwd)
        type: Directory
EOF